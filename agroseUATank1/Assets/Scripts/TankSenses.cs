﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankSenses : MonoBehaviour
{

    // 1.0f is normal hearing.
    public float hearingModifier = 1.0f;
    public float fieldofView = 45.0f;
    public float viewDistance = 10.0f;

    public bool CanSee (TankData otherTank)
    {
        //Handle FOV (field of View)
        // Find angle to other tank.
        Vector3 vectorToOtherTank = otherTank.transform.position - transform.position;

        //Find angle between vector and other tank
        float viewAngle = Vector3.Angle(vectorToOtherTank, transform.forward);

        // If that angle is > fieldofView out of Periphial vision, so we can't see it.
        if (viewAngle > fieldofView)
        {
            return false;
        }

        //Handle LOS (line of site)


        //Raycast mathematically calculate a line from mtank to tank for view distance.
        RaycastHit hit;

        //First object ray hits the other tank, we can see it
        if (Physics.Raycast(transform.position, vectorToOtherTank, out hit, viewDistance))
        {
            // Get data of what I hit
            TankData viewedObjectData = hit.collider.gameObject.GetComponent<TankData>();
            // If that is the object I'm looking for
            if (viewedObjectData == otherTank)
            {
                // then we saw it.
                return true;
            }
        }

        //Otherwise we can't see it.
        return false;
    }

   public bool CanHear(TankData otherTank)
    {

        // Get the noisemaking ability of the other tank.
       NoiseMaker otherNoise = otherTank.gameObject.GetComponent<NoiseMaker>();
        // If it does not exist, then we can't hear it.
        if (otherNoise == null)
        {
            return false;
        }
        else
        {
            // If it does exist, check if we are closer than (volume * volumeModifier)
            if (Vector3.Distance(otherTank.transform.position, transform.position) <= otherNoise.soundVolume * hearingModifier)
            {
                //close enough to hear, return true
                return true;
            }
        }

        // If outside of function and havent heard it yet, can't hear it
        return false;

    }
}
