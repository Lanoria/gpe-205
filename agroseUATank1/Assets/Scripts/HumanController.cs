﻿// Using uses libraries.
// Collections uses various collections of objects.
// This whole script collects decisions and movements.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This is a child class of controller.
public class HumanController : Controller
{
    public TankData pawn;
    public TankMover mover;
    public TankShooter shooter;
    public enum InputType
    {
        WASD, Arrows
            

    }

    public InputType input;
   // Start is called before the first frame update
    public override void Start()
    
   {
        
        GameManager.instance.players.Add(this);
   }

    public void OnDestroy()
    {
        GameManager.instance.players.Remove(this);
    }

    // Update is called once per frame
    public override void Update()
    
    {

        // Handle movement.
        
        // Assume movement is 0 -- no movement.

        // Make variable represents direction moved.
        Vector3 directionToMove = Vector3.zero;


        switch (input)
        {
            case InputType.WASD:
                // Check to see if player pressed w.
                // Move sharktank forward.
                // Z for z axis.
                if (Input.GetKey(KeyCode.W))
                {
                    directionToMove += pawn.transform.forward;
                }

                // Check to see if player pressed s.
                // Move sharktank bacwards.
                // Z for z axis.
                else if (Input.GetKey(KeyCode.S))
                {
                    directionToMove -= pawn.transform.forward;
                   
                }

                // Check if player press D.
                // Move Sharktank along x axis.
                if (Input.GetKey(KeyCode.D))
                {
                    mover.Rotate(pawn.rotateSpeed);
                }

                // Check if player press A.
                //Move Sharktank along x axis.
                else if (Input.GetKey(KeyCode.A))
                {
                    mover.Rotate(-pawn.rotateSpeed);
                }
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    shooter.Shoot();
                }
                break;
            case InputType.Arrows:

                // Check to see if player pressed w.
                // Move sharktank forward.
                // Z for z axis.
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    directionToMove += pawn.transform.forward;
                    
                }

                // Check to see if player pressed s.
                // Move sharktank bacwards.
                // Z for z axis.
                else if (Input.GetKey(KeyCode.DownArrow))
                {
                    directionToMove -= pawn.transform.forward;
                    
                }

                // Check if player press D.
                // Move Sharktank along x axis.
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    mover.Rotate(pawn.rotateSpeed);
                }

                // Check if player press A.
                //Move Sharktank along x axis.
                else if (Input.GetKey(KeyCode.LeftArrow))
                {
                    mover.Rotate(-pawn.rotateSpeed);
                }
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    shooter.Shoot();
                }
                break;
        }

        mover.Move(directionToMove);
        
    }
}
