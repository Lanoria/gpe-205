﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Aggresive : AI_Controller
{
    public float attackRadius;
    // Start is called before the first frame update
    

    protected override void Chase()
    {

        data.mover.RotateTowards(sight.target.position);
        data.mover.Move(transform.forward);
    }

    protected override void Search()
    {
        
        
        
            data.mover.RotateTowards(sight.lastPosition.transform.position);
            data.mover.Move(transform.forward);
        
    }


    // Update is called once per frame

    protected override void Update()
    {

        switch (Aistates)
        {
            case AIStates.Idle:
                Idle();
                break;
            case AIStates.Patrol:
                Patrol();
                if (sight.canSeePlayer())
                {
                    changeState(AIStates.Chase);
                }
                break;
            case AIStates.Chase:
                Chase();
                if (Vector3.Distance(transform.position, sight.target.position) < attackRadius)
                {
                    changeState(AIStates.Attack);
                }
                break;
            case AIStates.Attack:
                Attack();
                if (sight.canSeePlayer() && Vector3.Distance(transform.position, sight.lastPosition.transform.position) > attackRadius)
                {
                    changeState(AIStates.Chase);
                }

                if (sight.canSeePlayer() == false)
                {
                    changeState(AIStates.Search);
                }

                break;
            case AIStates.Search:
                Search();
                if (sight.canSeePlayer() && Vector3.Distance(transform.position, sight.lastPosition.transform.position) > attackRadius)
                {
                    changeState(AIStates.Chase);
                }

                if (sight.canSeePlayer() && Vector3.Distance(transform.position, sight.lastPosition.transform.position) < attackRadius)
                {
                    changeState(AIStates.Attack);
                }

                if (Vector3.Distance(transform.position, sight.lastPosition.transform.position) < closeEnough)
                {
                    changeState(AIStates.Patrol);
                }
                break;

                
           

        }


    }
}
