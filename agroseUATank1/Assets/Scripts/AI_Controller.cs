﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public abstract class AI_Controller : MonoBehaviour
{
    public Transform[] waypoints;
    public TankData data;
    private int currentWaypoint = 0;
    public float closeEnough;
    public Transform target;
    public float avoidanceTime;
    private float exitTime;
    public LayerMask avoidanceLayer;
    public Transform rayCastTransform;
    public Sight sight;

    public enum AIStates
    {
        Idle, Patrol, Chase, Attack, Search, Flee, Shout

    }

    public AIStates Aistates;
    public AvoidanceStates avoidanceStates;
    public enum AvoidanceStates
    {
        None, turnToAvoid, moveToAvoid
    }

    bool CanMove()
    {
        RaycastHit hit = default;
        Debug.DrawRay(rayCastTransform.position, transform.forward, Color.red);
        if (Physics.Raycast(rayCastTransform.position, transform.forward, 20f, avoidanceLayer))
        {
         //   Debug.Log("I can see obstacle");
            return false;
        }
        return true;
    }
    void DoAvoidance()
    {
        
        switch (avoidanceStates)
        {
            case AvoidanceStates.None:
                if (CanMove())
                {
                    data.mover.RotateTowards(target);
                    data.mover.Move(transform.forward); 
                }
                else
                {
                    avoidanceStates = AvoidanceStates.turnToAvoid;
                }
                break;
            case AvoidanceStates.turnToAvoid:
                data.mover.Rotate(data.rotateSpeed);
                if (CanMove())
                {
                    avoidanceStates = AvoidanceStates.moveToAvoid;

                    exitTime = avoidanceTime;
                }
                break;
            case AvoidanceStates.moveToAvoid:

                //if we can move forward do so
                if (CanMove())
                {
                    exitTime -= Time.deltaTime;
                    data.mover.Move(transform.forward);
                }

                if (exitTime <= 0)
                {
                    avoidanceStates = AvoidanceStates.None;
                }
                break;

        }
        
    }

    protected void changeState(AIStates newState)
    {
        Aistates = newState;
    }
    // Update is called once per frame
    protected virtual void Update()
    {
        switch (Aistates)
        {
            case AIStates.Idle:
                Idle();
                break;
            case AIStates.Patrol:
                Patrol();
                break;
            case AIStates.Chase:
                Chase();
                break;
            case AIStates.Attack:
                Attack();
                break;
            case AIStates.Search:
                Search();
                break;
            case AIStates.Flee:
                Flee();
                break;
            case AIStates.Shout:
                Shout();
                break;

        }

        DoAvoidance();
    }

    protected virtual void Idle()
    {

    }

    protected virtual void Patrol()
    {
        // Debug.Log("Calling Patrol");
        //if close to waypoint.
        Debug.Log(this.name + "'s current waypoint is: " + waypoints[currentWaypoint]);
        if (Vector3.Distance(waypoints[currentWaypoint].position, transform.position) <= closeEnough)
        {

          //  Debug.Log("Close Enough");
            //Advance to the next waypoint.
            if (currentWaypoint < waypoints.Length - 1)
            {
                currentWaypoint++;

            }
            else
            {
                currentWaypoint = 0;
            }

            target = waypoints[currentWaypoint];
        }
        data.mover.RotateTowards(target);


        data.mover.Move(transform.forward);

    }

    protected virtual void Chase()
    {

    }

    protected virtual void Attack()
    {
     //   Debug.Log("I am shooting with attack");
        data.shooter.Shoot();
    }
    protected virtual void Search()
    {

    }
    protected virtual void Flee()
    {

    }
    protected virtual void Shout()
    {

    }

    public void Start()
    {
        sight = GetComponent<Sight>();
        GameManager.instance.enemies.Add(this);
    }

}
