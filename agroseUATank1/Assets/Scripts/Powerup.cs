﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Powerup : MonoBehaviour
{
    
    public float speedModifier;
    public int maxHealthModifier;
    public float fireRateModifier;
    public float duration;
    public bool isPermanent;
    public int healthModifier;
    public int maxHealth;




    public void OnActivate(TankData target)
    {
        target.moveSpeed += speedModifier;
        target.health.health += healthModifier;
        target.health.healthMax += maxHealthModifier;
        target.fireRate += fireRateModifier;
    }

    
    public void OnDeactivate(TankData target)
    {
        target.moveSpeed -= speedModifier;
        target.health.health -= healthModifier;
        target.health.healthMax -= maxHealthModifier;
        target.fireRate -= fireRateModifier;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

       
    }
}
