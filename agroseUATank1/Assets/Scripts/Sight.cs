﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sight : MonoBehaviour
{
    public float viewRadius;
    public float viewAngle;
    public LayerMask playerLayer;
    public Transform target;
    public GameObject lastPosition;
    public bool canSeePlayer()
    {
        Collider[] collider = Physics.OverlapSphere(transform.position, viewRadius, playerLayer);
        if (collider.Length > 0)
        {
            foreach (Collider player in collider)
            {
                lastPosition = new GameObject();
            }
            

            target = collider[0].transform;

            lastPosition.transform.position = target.position;
            if (Vector3.Angle(transform.position, target.position) <= viewAngle / 2)
            {
                GetComponent<AI_Controller>().target = target;
                return true;
            }


        }
        return false;
    }

    
}
