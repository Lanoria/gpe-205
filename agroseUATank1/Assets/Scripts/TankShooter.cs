﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TankShooter : MonoBehaviour
{
    public Transform FireTransform;
    public GameObject Bullet;
    private float Timer;
    public TankData data;
    public AudioSource audioSource;
    public AudioClip shootSFX;
    public void Shoot()
    {
        audioSource.clip = shootSFX;
        audioSource.Play();
      //  Debug.Log("Tank Shooter working");
        Timer -= Time.deltaTime;
        if(Timer <= 0)
        {
            GameObject bullet = Instantiate(Bullet, FireTransform.position, FireTransform.rotation);
            bullet.GetComponent<BulletScript>().SetOwner(data);
            Timer = data.fireRate;
        }
        
        
    }

    public void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
}
