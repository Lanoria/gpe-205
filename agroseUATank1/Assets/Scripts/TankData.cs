﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{
    // Data
    public float moveSpeed;
    public float rotateSpeed;
    public float defaultMoveSpeed;
    public float fireRate;
    public float damage;
    public AudioClip deathSound;
    public AudioSource audioSource;
    public int lives;
    public int score;
    public int hiscore;
    



    //Components
    [HideInInspector]
    public TankMover mover;

    [HideInInspector]
    public TankShooter shooter;

    [HideInInspector]
    public TankHealth health;

    
    public Camera camera;

    

    // Start is called before the first frame update
    void Start()
    {
        mover = GetComponent<TankMover>();
        shooter = GetComponent<TankShooter>();
        health = GetComponent<TankHealth>();
        audioSource = GetComponent<AudioSource>();
    }

   



}


