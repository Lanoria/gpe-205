﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smart : AI_Controller
{
    public float attackRadius;
    // Start is called before the first frame update
    

    protected override void Chase()
    {
        data.mover.RotateTowards(sight.target);
        data.mover.Move(transform.forward);
    }

    protected override void Search()
    {



        data.mover.RotateTowards(sight.lastPosition.transform.position);
        data.mover.Move(transform.forward);

    }
    protected override void Flee()
    {
        Vector3 directionToPlayer = sight.target.position - transform.position;
        directionToPlayer = directionToPlayer * -1;
        data.mover.RotateTowards(directionToPlayer);
        data.mover.Move(transform.forward);
    }

    protected override void Shout()
    {
        Transform closestTank = null;
        float distance = 0;
        foreach (AI_Controller Vehicles in GameManager.instance.enemies)
        {
            if (Vehicles != this)
            {
                if (closestTank == null)
                {
                    closestTank = Vehicles.transform;
                    distance = Vector3.Distance(transform.position, closestTank.position);
                }

                else
                {
                    if (Vector3.Distance(transform.position, Vehicles.transform.position) < distance)
                    {
                        closestTank = Vehicles.transform;
                        distance = Vector3.Distance(transform.position, closestTank.position);
                    }
                }
            }
        }
        closestTank.GetComponent<AI_Controller>().target = target;
    }

    // Update is called once per frame

    protected override void Update()
    {

        switch (Aistates)
        {
            case AIStates.Idle:
                Idle();
                break;
            case AIStates.Patrol:
                Patrol();
                if (sight.canSeePlayer())
                {
                    changeState(AIStates.Chase);
                }
                break;
            case AIStates.Chase:
                Chase();
                if (Vector3.Distance(transform.position, sight.target.position) < attackRadius)
                {
                    changeState(AIStates.Attack);
                }
                break;
            case AIStates.Attack:
                Attack();
                if (sight.canSeePlayer() && Vector3.Distance(transform.position, sight.lastPosition.transform.position) > attackRadius)
                {
                    changeState(AIStates.Chase);
                }

                if (sight.canSeePlayer() == false)
                {
                    changeState(AIStates.Search);
                }

                if (data.health.health < data.health.healthMax / 2)
                {
                    changeState(AIStates.Shout);
                }

                break;
            case AIStates.Search:
                Search();
                if (sight.canSeePlayer() && Vector3.Distance(transform.position, sight.lastPosition.transform.position) > attackRadius)
                {
                    changeState(AIStates.Chase);
                }

                if (sight.canSeePlayer() && Vector3.Distance(transform.position, sight.lastPosition.transform.position) < attackRadius)
                {
                    changeState(AIStates.Attack);
                }

                if (Vector3.Distance(transform.position, sight.lastPosition.transform.position) < closeEnough)
                {
                    changeState(AIStates.Patrol);
                }
                break;
            case AIStates.Flee:
                Flee();
                if (sight.canSeePlayer() == false)
                {
                    changeState(AIStates.Patrol);
                }
                break;
            case AIStates.Shout:
                Shout();
                {
                    if (sight.canSeePlayer() && Vector3.Distance(transform.position, sight.lastPosition.transform.position) > attackRadius)
                    {
                        changeState(AIStates.Chase);
                    }
                    if (sight.canSeePlayer() && Vector3.Distance(transform.position, sight.lastPosition.transform.position) < attackRadius)
                    {
                        changeState(AIStates.Attack);
                    }
                }
                break;
            
                
            



        }
    }


}
