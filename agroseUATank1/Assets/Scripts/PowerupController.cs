﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour
{
    public TankData data;
    //internal class Powerup

    //{


    //}
    public List<Powerup> powerups;

    

    // Start is called before the first frame update
    void Start()
    {
        powerups = new List<Powerup>();
        data = GetComponent<TankData>();
                        
    }
    public void Add(Powerup powerup)
    {
        // Run the OnActivate function of the powerup
        powerup.OnActivate(data);

        // Only add permananet power ups to the list
        if (!powerup.isPermanent)
        {
            powerups.Add(powerup);
        }

    }

   

    // Update is called once per frame
    void Update()
    {
        // Loop through all the powers in the List

        foreach (Powerup power in powerups)
        {
            // Subtract from the timer
            power.duration -= Time.deltaTime;

            // If time is up, deactivate the powerup and remove it from the List
            if (power.duration <= 0)
            {
                power.OnDeactivate(data);
                powerups.Remove(power);
            }

        }

        // Create an List to hold our expired powerups
    List<Powerup> expiredPowerups = new List<Powerup>();

        // Loop through all the powers in the List
        foreach (Powerup power in powerups)
        {
            // Subtract from the timer
            power.duration -= Time.deltaTime;

            // Assemble a list of expired powerups
            if (power.duration <= 0)
            {
                expiredPowerups.Add(power);
            }
        }
        // Now that we've looked at every powerup in our list, use our list of expired powerups to remove the expired ones.
        foreach (Powerup power in expiredPowerups)
        {
            power.OnDeactivate(data);
            powerups.Remove(power);
        }
        // Since our expiredPowerups is local, it will *poof* into nothing when this function ends,
        /// but let's clear it to learn how to empty an List
        expiredPowerups.Clear();


        
        }
    
    

}

