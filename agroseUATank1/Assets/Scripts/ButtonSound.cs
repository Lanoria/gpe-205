﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSound : MonoBehaviour
{
    public AudioClip audioClip;
    public AudioSource audioSource;
    public void onClickSound()
    {
        audioSource.clip = audioClip;
        audioSource.Play();
    }
}
