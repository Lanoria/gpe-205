﻿// Using uses libraries.
// Collections uses various collections of objects.
using System.Collections;
using System.Collections.Generic;
// Contains classes and code from unity.
using UnityEngine;

// Inheritance class Hello mono inherits from class.
// Parent and child class
public class HelloMonoBehavior : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
