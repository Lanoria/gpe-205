﻿// Using uses libraries.
// Collections uses various collections of objects.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This is the parent class controller.
// Mono is example of inheritance.
public class Controller : MonoBehaviour
{
    // Start is called before the first frame update
    public virtual void Start()
    {
        
    }

    // Update is called once per frame
    public virtual void Update()
    {
        
    }
}
