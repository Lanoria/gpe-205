﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    // Create variable instance.
    public static GameManager instance;
    // make a list of players and enemies for controllers
    public List<HumanController> players;
    public List<AI_Controller> enemies;
    // made int for lives and player scores
    
    // made text for lives and scores
    public Text livesText;
    public Text scoreText;
    // made a bool for map level of day and randome level
    public bool levelOfTheDay;
    public bool randomLevel;
    // made audiomixer for scenes
    public AudioMixer audiomixer;
    //made slideres for volume, sfx, and music
    public Slider MasterVolume;
    public Slider SFX;
    public Slider Music;
    // made a bool for 2 player game
    public bool twoPlayer;
    //made a map generator
    private MapGenerator mapGenerator;
    //making spawnpoints and enemy spawns as well. square brackets make arrays
    public PlayerSpawnPoint[] playerSpawnPoints;
    public EnemySpawnPoint[] enemySpawnPoints;
    public GameObject[] playerPrefab;
    public GameObject[] enemyPrefab;
    public HumanController playerOne;
    public HumanController playerTwo;
    public GameObject[] enemyWaypoints;
    public bool isLevelLoaded = false;
    public int maxEnemy;
    public int P1HiScore;
    public int P2HiScore;



    //Awake called when object is created.

    private void Awake()
    {
        if (instance == null)
        {
            // Variable instance = this game object.
            instance = this;

            
        }
        else
        {
            Destroy(gameObject);
        }

        

        DontDestroyOnLoad(this.gameObject);

       
    }

    private void Start()
    {
        // Create a list of enemies.
        enemies = new List<AI_Controller>();
        // Create a list of players.
        players = new List<HumanController>();

        mapGenerator = GetComponent<MapGenerator>();
    }

    // Start is called before the first frame update
    void StartGame()
    {
        isLevelLoaded = true;
        
        
        

        mapGenerator.InitializeMap();
        playerSpawnPoints = GameObject.FindObjectsOfType<PlayerSpawnPoint>();
        enemySpawnPoints = GameObject.FindObjectsOfType<EnemySpawnPoint>();

        enemyWaypoints = GameObject.FindGameObjectsWithTag("Waypoint");
        Debug.Log(enemyWaypoints.Length);


        for (int i = 0; i < maxEnemy; i++)
        {
            int randomTank = UnityEngine.Random.Range(0, enemyPrefab.Length);
            GameObject E = Instantiate(enemyPrefab [randomTank], enemySpawnPoints[i].transform.position, enemySpawnPoints[i].transform.rotation);
            enemies.Add(E.GetComponent<AI_Controller>());

            AI_Controller ai = E.GetComponent <AI_Controller>();

            ai.waypoints = new Transform[4];
            ai.waypoints[0] = enemyWaypoints[UnityEngine.Random.Range(0, enemyWaypoints.Length - 1)].transform;
            ai.waypoints[1] = enemyWaypoints[UnityEngine.Random.Range(0, enemyWaypoints.Length - 1)].transform;
            ai.waypoints[2] = enemyWaypoints[UnityEngine.Random.Range(0, enemyWaypoints.Length - 1)].transform;
            ai.waypoints[3] = enemyWaypoints[UnityEngine.Random.Range(0, enemyWaypoints.Length - 1)].transform;
            ai.target = ai.waypoints[0];

        }
        // Set up two player game
        int randomPlayerPosition = UnityEngine.Random.Range(0, playerSpawnPoints.Length);
        GameObject P1 = Instantiate(playerPrefab[1], playerSpawnPoints[randomPlayerPosition].transform.position, playerSpawnPoints[randomPlayerPosition].transform.rotation);
        Vector3 myPos = P1.transform.position;
        myPos.y = 2f;
        P1.transform.position = myPos;
        playerOne.pawn = P1.GetComponent<TankData>();
        playerOne.mover = P1.GetComponent<TankMover>();
        playerOne.shooter = P1.GetComponent<TankShooter>();
        playerOne.input = HumanController.InputType.WASD;
        players.Add(playerOne);
        loadScore(playerOne);
        
        // set players 2 score to 0 
        // player 2 hi score
        // set player health to full health
        // set player lives to 3
        Text[] textChildren = P1.GetComponentsInChildren<Text>();
        foreach(Text text in textChildren)
        {
            if (text.gameObject.tag == "livestext")
            {
                livesText = text;
            }
            if (text.gameObject.tag == "scoretext")
            {
                scoreText = text;
            }

        }
        
        
        if (twoPlayer)
        {
            // Set up two player game
            int randomPlayerTwoPosition = UnityEngine.Random.Range(0, playerSpawnPoints.Length);
           GameObject P2 = Instantiate(playerPrefab[1], playerSpawnPoints[randomPlayerTwoPosition].transform.position, playerSpawnPoints[randomPlayerTwoPosition].transform.rotation);
            Vector3 myPos2 = P2.transform.position;
            myPos2.y = 2f;
            P2.transform.position = myPos2;
            playerTwo.pawn = P2.GetComponent<TankData>();
            playerTwo.mover = P2.GetComponent<TankMover>();
            playerTwo.shooter = P2.GetComponent<TankShooter>();
            playerTwo.input = HumanController.InputType.Arrows;

            players.Add(playerTwo);
            loadScore(playerTwo);
            // set players 2 score to 0 
            // player 2 hi score
            // set player health to full health
            // set player lives to 3

            Text[] textChildren2 = P2.GetComponentsInChildren<Text>();
            foreach (Text text in textChildren2)
            {
                if (text.gameObject.tag == "livestext")
                {
                    livesText = text;
                }
                if (text.gameObject.tag == "scoretext")
                {
                    scoreText = text;
                }

            }

            P2.GetComponent<TankData>().camera.rect = new Rect(.5f, 0f, .5f, 1f);
            P1.GetComponent<TankData>().camera.rect = new Rect(0, 0, .5f, 1f);
        }

        

        // spawn pick ups

    }

    public void SpawnPlayer(TankData Player)
    {
        int newPosition = UnityEngine.Random.Range(0, playerSpawnPoints.Length);

        Vector3 playerPosition = Player.transform.position;
        playerPosition = playerSpawnPoints[newPosition].transform.position;
        Player.transform.position = playerPosition;

        Player.health.health = Player.health.healthMax;
        Player.moveSpeed = Player.defaultMoveSpeed;

        Player.GetComponent<MeshRenderer>().enabled = true;
        Player.GetComponent<Collider>().enabled = true;

    }

    public void SetTwoPlayer(Toggle toggle)
    {
        twoPlayer = toggle.isOn;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateUI();

        if (isLevelLoaded == false && SceneManager.GetActiveScene().name == "Main")
        {
           
            StartGame();
            isLevelLoaded = true;
        }
    }
     
    public void addScore(HumanController player, int score)
    {
        player.pawn.score += score;
        if (player.pawn.score > player.pawn.hiscore)
        {
            player.pawn.hiscore = player.pawn.score;
            PlayerPrefs.SetInt("HiScore" + players.IndexOf(player).ToString(), player.pawn.hiscore);
        }
       
    }

    public void loadScore(HumanController player)
    {
        Debug.Log(player);
        Debug.Log(player.pawn);
        Debug.Log(player.pawn.hiscore);
        if (PlayerPrefs.HasKey("HiScore" + players.IndexOf(player).ToString()))
        {
            player.pawn.hiscore = PlayerPrefs.GetInt("HiScore" + players.IndexOf(player).ToString()); 
        }
        else
        {
            PlayerPrefs.SetInt("HiScore" + players.IndexOf(player).ToString(), 0);
        }
    }
    void UpdateUI()
    {

        if (SceneManager.GetActiveScene() .name == "Main" && isLevelLoaded)
        {

            livesText.text = players[0].pawn.lives.ToString();
            scoreText.text = players[0].pawn.score.ToString();
            

            if (twoPlayer)
            {
                livesText.text = players[1].pawn.lives.ToString();
                scoreText.text = players[1].pawn.score.ToString();
            }
        }


        
    }

    public void setLevelOfTheDay(Toggle toggle)
    {
        levelOfTheDay = toggle.isOn;
    }

    public void setrandomLevel(Toggle toggle)
    {
        randomLevel = toggle.isOn;
    }

    public void setMasterVolume()
    {
        float volume = 0;
        volume = (80 * MasterVolume.value) - 80;
        audiomixer.SetFloat("Master", volume);
    }

    public void setSFX()
    {
        float volume = 0;
        volume =  (80 * SFX.value) - 80;
        audiomixer.SetFloat("SoundEffects", volume);
    }

    public void setMusic()
    {
        float volume = 0;
        volume = (80 * Music.value) - 80; ;
        audiomixer.SetFloat("Background", volume);
    }
}
