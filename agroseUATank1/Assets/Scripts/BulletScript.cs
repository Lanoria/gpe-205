﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public Rigidbody bullet;
    public float speed;
    public AudioSource audioSource;
    public AudioClip bulletHit;
    private float damageAmount;
    private void Start()
    {

        
      //  Debug.Log("Bullet is working");
        bullet = GetComponent<Rigidbody>();
        bullet.velocity = transform.forward * speed;
    }

    public void SetOwner(TankData Owner)
    {
        damageAmount = Owner.damage;
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<TankHealth>())
        {
            // Call Tank Damage
            other.GetComponent<TankHealth>().TakeDamage(damageAmount);
           
        }

        audioSource.clip = bulletHit;
        audioSource.Play();

        this.GetComponent<MeshRenderer>().enabled = false;
        Destroy(this.gameObject, .2f);

    }


}
