﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    [SerializeField] private int numberOfRows;
    [SerializeField] private int numberOfColumns;
    [SerializeField] private GameObject[] mapTiles;
    [SerializeField] private float rowOffset;
    [SerializeField] private float columnOffset;
    [SerializeField] private bool levelOfTheDay;
    [SerializeField] private bool randomLevel;
    [SerializeField] private int Seed;


    // Start is called before the first frame update
    public void InitializeMap()
    {
        if (GameManager.instance.levelOfTheDay)
        {

            int Date = (System.DateTime.Now.Month) + (System.DateTime.Now.Year) +(System.DateTime.Now.Day);
            Random.InitState(Date);
        }
        else
        {
            if (GameManager.instance.randomLevel)
            {
                Seed = Random.Range(1, 100000000);
            }
            Random.InitState(Seed);
        }
        generateMap();
    }

    private GameObject randomTile()
    {
        GameObject randomTile = mapTiles[Random.Range(0, mapTiles.Length)];
        return randomTile;
    }

    public void generateMap()
    {
        for (int Rows = 0; Rows < numberOfRows; Rows ++)
        {
            for (int Columns =0; Columns < numberOfColumns; Columns++)
            {
                GameObject newMapTile = Instantiate(randomTile());

                newMapTile.transform.position = new Vector3(Rows * rowOffset, 0, Columns * columnOffset);
            }
        }



    }

}
