﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    private TankHealth tankHealth;

    private void Setup (TankData tankdata)
    {
        this.tankHealth = tankHealth;

        tankHealth.OnHealthChanged += tankHealth_OnHealthChanged;
    }

    private void tankHealth_OnHealthChanged(object sender, System.EventArgs e)
    {
        transform.Find("Bar").localScale = new Vector3(tankHealth.GetHealthPercent(), 1);
      //  throw new System.NotImplementedException();
    }


    // Update is called once per frame
    private void Update()
    {
        // transform.Find{ "Bar"}.localscale = new Vector3(tankHealth.GetHealthPercent(), 1);
    }
}
