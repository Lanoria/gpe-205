﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pick_Up : MonoBehaviour
{
    public Powerup powerup;
    public AudioClip feedback;
    public GameObject pickupPrefab;
    public float spawnDelay;
    private float nextSpawnTime;
    private Transform tf;
    public GameObject spawnedPickup = null;
    public float destroyDelay;
    public AudioSource audioSource;
    public AudioClip audioClip;
    public float timeDestroyed;



    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Something colided with me");
        if (spawnedPickup != null)
        {
            
            PowerupController powCon = other.GetComponent<PowerupController>();
            Debug.Log("Who collided with me: " + powCon);
            if (powCon)
            {
                powCon.Add(spawnedPickup.GetComponent<Powerup>());
            }

            if (feedback != null)
            {
                AudioSource.PlayClipAtPoint(feedback, tf.position, 1.0f);
            }

            Destroy(spawnedPickup);
            powerup = null;

        }
        
    }


    // Start is called before the first frame update
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();
        nextSpawnTime = 0;

        if (spawnedPickup != null)
        {
            Destroy(spawnedPickup, destroyDelay);
        }
        
        
    }





    // Update is called once per frame
    void Update()
    {


        // If it is there is nothing spawns
        if (spawnedPickup == null)
        {
            Debug.Log("I don't have a pickup");
            // And it is time to spawn
            if (Time.time > nextSpawnTime)

            {
                Debug.Log("I can spawn a pickup");
                // Spawn it and set the next time
                spawnedPickup = Instantiate(pickupPrefab, transform.position, transform.rotation);
                spawnedPickup.transform.SetParent(this.transform, false);
                nextSpawnTime = Time.time + spawnDelay;
            }
        }
        else
        {
            Debug.Log("I have a pick up");
            // Otherwise, the object still exists, so postpone the spawn
            nextSpawnTime = Time.time + spawnDelay;
        }
    }
}
