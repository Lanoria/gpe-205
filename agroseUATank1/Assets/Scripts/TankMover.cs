﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TankMover : MonoBehaviour
{
    private TankData data;
    private CharacterController cc;
    
    
    

    public void Rotate(float rotateSpeed)
    {
        //TODO: Go back to milestone 1 to learn Rotate


        Vector3 rotateVector = Vector3.up;
        rotateVector *= (rotateSpeed *= Time.deltaTime);

        transform.Rotate(rotateVector, Space.Self);
    }
    public void RotateTowards(Vector3 direction)
    {
        Quaternion targetRotation = Quaternion.LookRotation(direction.normalized, Vector3.up);
        targetRotation.x = 0;
        targetRotation.z = 0;
        // start with your position rotate slowly toward other tank position
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, data.rotateSpeed * Time.deltaTime);
        // Todo: Fix rotation part of script
    }

    public void RotateTowards(Transform target)
    {
        Vector3 vectorToTarget;

        // The vector to our target is the Difference
        // between the target position and our position
        vectorToTarget = (target.position - transform.position).normalized;
        // Debug.Log(vectorToTarget.normalized);
        // Find the Quaterion that looks down the Vector.
        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget, Vector3.up);
        targetRotation.x = 0;
        targetRotation.z = 0;





        // start with your position rotate slowly toward other tank position
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, data.rotateSpeed * Time.deltaTime);



    }



    void Start()
    {
        data = GetComponent<TankData>();
        cc = GetComponent<CharacterController>();
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Creating function to move sharktank.
    public void Move(Vector3 directionToMove)
    {

        Debug.Log(directionToMove);
        cc.SimpleMove(directionToMove * data.moveSpeed * Time.deltaTime);
    }
}
