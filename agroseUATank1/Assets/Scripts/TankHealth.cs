﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TankHealth : MonoBehaviour
{
    public event EventHandler OnHealthChanged;

    public float health;
    public float healthMax;
    public TankData data;

    // make a constuctor with an integer for health

    public TankHealth (int healthMax)
    {
        this.health = healthMax;
        health = healthMax;
    }

        public float GetHealth()
    {
        return health;
    }

    public float GetHealthPercent()
    {
        return health / healthMax;
    }

    // make a constructor with an integer to get Damage
    public void TakeDamage(float damageAmount)
    {
        health -= damageAmount;
        if (health <= 0)
        {
            Kill();
        }
        if (OnHealthChanged != null) OnHealthChanged(this, EventArgs.Empty);
    }

    private void Kill()
    {
        if (this.GetComponent<AI_Controller>())
        {
            GameManager.instance.enemies.Remove(this.GetComponent<AI_Controller>());
            data.audioSource.clip = data.deathSound;
            data.audioSource.Play();
            this.GetComponent<MeshRenderer>().enabled = false;
            this.GetComponent<Collider>().enabled = false;
            Destroy(this.gameObject, .2f);
        }
        else
        {
            data.audioSource.clip = data.deathSound;
            data.audioSource.Play();
            this.GetComponent<MeshRenderer>().enabled = false;
            this.GetComponent<Collider>().enabled = false;
            
            GameManager.instance.SpawnPlayer(data);
           
            
        }
    }

    public void Heal(float healAmount)
    {
        health += healAmount;
        if (health > healthMax) health = healthMax;
        if (OnHealthChanged != null) OnHealthChanged(this, EventArgs.Empty);
    }

       

        
}
