﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Navigation : MonoBehaviour
{
    public GameObject OptionsMenu;

    public void StartGame()
    {
        SceneManager.LoadScene("Main");

    }

    public void OptionMenu()
    {
        OptionsMenu.SetActive(!OptionsMenu.activeSelf);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
