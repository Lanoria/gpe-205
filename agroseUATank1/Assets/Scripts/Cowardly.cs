﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Cowardly : AI_Controller
{
    public Transform myBuddy;

    protected override void Flee()
    {
        Vector3 directionToPlayer = sight.target.position - transform.position;
        directionToPlayer = directionToPlayer * -1;
        data.mover.RotateTowards(directionToPlayer);
        data.mover.Move(transform.forward);

        
    }

    protected override void Search()
    {
        Transform closestTank = null;
        float distance = 0;
        foreach (AI_Controller Vehicles in GameManager.instance.enemies)
        {
            if (Vehicles != this)
            {
                if (closestTank == null)
                {
                    closestTank = Vehicles.transform;
                    distance = Vector3.Distance(transform.position, closestTank.position);
                }

                else
                {
                    if (Vector3.Distance(transform.position, Vehicles.transform.position) < distance)
                    {
                        closestTank = Vehicles.transform;
                        distance = Vector3.Distance(transform.position, closestTank.position);
                    }
                }
            }
        }
        myBuddy = closestTank;
    }

    protected override void Chase()
    {
        data.mover.RotateTowards(myBuddy);
        data.mover.Move(transform.forward);
    }

    protected override void Update()
    {
        switch (Aistates)
        {
            case AIStates.Idle:
                Idle();
                break;
            case AIStates.Patrol:
                Patrol();
                if (sight.canSeePlayer())
                {
                    changeState(AIStates.Flee);
                }
                break;
            case AIStates.Chase:
                Chase();
                if(!sight.canSeePlayer())
                {
                    changeState(AIStates.Patrol);
                }
                break;
            case AIStates.Search:
                Search();
                if (myBuddy != null)
                {
                    changeState(AIStates.Chase);
                }
                break;
            case AIStates.Flee:
                Flee();
                if(myBuddy == null)
                {
                    changeState(AIStates.Search);
                }
                break;
            

        }
    }


}
